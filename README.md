# Checkerboard

A pawn can move on 10x10 chequerboard horizontally, vertically and diagonally by these rules:

1. 3 tiles moving North (N), West (W), South (S) and East (E)
2. 2 tiles moving NE, SE, SW and NW
3. Moves are only allowed if the ending tile exists on the board
4. Starting from initial position, the pawn can visit each cell only once

On the following picture you can see the initial position in black, the legal next positions in dark grey and the illegal ones in red.

![](./img/task.png)


# Solution

We can split 10x10 checkerboard to 4 5x5 boards. And make path that covers these small boards. 
E.g.:
    
        ┌────┬────┬────┬────┬────┐
        │ 1  │ 17 │ 24 │ 2  │ 18 │
        ├────┼────┼────┼────┼────┤
        │ 22 │ 14 │ 5  │ 21 │ 13 │
        ├────┼────┼────┼────┼────┤
        │ 25 │ 8  │ 11 │ 16 │ 7  │
        ├────┼────┼────┼────┼────┤
        │ 4  │ 20 │ 23 │ 3  │ 19 │
        ├────┼────┼────┼────┼────┤
        │ 10 │ 15 │ 6  │ 9  │ 12 │
        └────┴────┴────┴────┴────┘
 
 At the end of this path we can make move North and get 1 cell upper starting point. 
 In this case we can combine 4 of these paths together just turned counter-clockwise on different angel to cover full board.
 Will use original path in bottom right corner, in top right corner will use path turned 90 degrees counter-clockwise ,
 in top left corner will use path turned 180 degrees counter-clockwise and in bottom-left corner will use path turned 270 degrees counter-clockwise.
 
                  ┼────┼
                  │ 75 │
                  ┼────┼   ...
           ...    │    │
                  ┼────┼────┼────┼────┼
                  │ 51 │ 26 │    │ 50 │
        ┼────┼────┼────┼────┼────┼────┼
        │100 │    │ 76 │ 1  │
        ┼────┼────┼────┼────┼
                       │    │  ...
             ...       ┼────┼
                       │ 25 │ 
                       ┼────┼

Also this full path is cyclic so you can move from last cell into start point. It helps implement building such path from any point of the board.
In fact path will be started from the same start point (5, 5) anyway but counting and setting values into resulting array will start only after finding desired starting point.

#Running solution

After checking out or unpacking the solution you can start it with the following command

        $ ./gradlew check run -Pappargs="<arguments>"
        :compileJava UP-TO-DATE
        :processResources UP-TO-DATE
        :classes UP-TO-DATE
        :compileTestJava UP-TO-DATE
        :processTestResources UP-TO-DATE
        :testClasses UP-TO-DATE
        :test UP-TO-DATE
        :check UP-TO-DATE
        :run
        
        
        BUILD SUCCESSFUL
        
        Total time: 1.254 secs
  
You can ask program to print help message that listed all available options
 
        $ ./gradlew check run -Pappargs="--help"
        :compileJava UP-TO-DATE
        :processResources UP-TO-DATE
        :classes UP-TO-DATE
        :compileTestJava UP-TO-DATE
        :processTestResources UP-TO-DATE
        :testClasses UP-TO-DATE
        :test UP-TO-DATE
        :check UP-TO-DATE
        :run
        
        Usage: PathFinder [options]
        Options:
        --help
           print this message
           Default: false
        --start
           coordinates of start point in format (x,y). If not passed start point
           will be chosen automatically
        --ui
           enables small swing table to demonstrate the process
           Default: false
        
        
        BUILD SUCCESSFUL
        
        Total time: 0.938 secs

If you run program without arguments start point will be taken automatically and result will be printed in console

        $ ./gradlew check run
        :compileJava UP-TO-DATE
        :processResources UP-TO-DATE
        :classes UP-TO-DATE
        :compileTestJava UP-TO-DATE
        :processTestResources UP-TO-DATE
        :testClasses UP-TO-DATE
        :test UP-TO-DATE
        :check UP-TO-DATE
        :run
        ┌─────┬─────┬─────┬────┬────┬────┬────┬────┬────┬────┐
        │ 62  │ 59  │ 56  │ 65 │ 60 │ 43 │ 38 │ 32 │ 44 │ 37 │
        ├─────┼─────┼─────┼────┼────┼────┼────┼────┼────┼────┤
        │ 69  │ 53  │ 73  │ 70 │ 54 │ 27 │ 46 │ 41 │ 28 │ 34 │
        ├─────┼─────┼─────┼────┼────┼────┼────┼────┼────┼────┤
        │ 57  │ 66  │ 61  │ 58 │ 75 │ 49 │ 30 │ 36 │ 48 │ 31 │
        ├─────┼─────┼─────┼────┼────┼────┼────┼────┼────┼────┤
        │ 63  │ 71  │ 55  │ 64 │ 72 │ 42 │ 39 │ 33 │ 45 │ 40 │
        ├─────┼─────┼─────┼────┼────┼────┼────┼────┼────┼────┤
        │ 68  │ 52  │ 74  │ 67 │ 51 │ 26 │ 47 │ 50 │ 29 │ 35 │
        ├─────┼─────┼─────┼────┼────┼────┼────┼────┼────┼────┤
        │ 85  │ 79  │ 100 │ 97 │ 76 │ 1  │ 17 │ 24 │ 2  │ 18 │
        ├─────┼─────┼─────┼────┼────┼────┼────┼────┼────┼────┤
        │ 90  │ 95  │ 83  │ 89 │ 92 │ 22 │ 14 │ 5  │ 21 │ 13 │
        ├─────┼─────┼─────┼────┼────┼────┼────┼────┼────┼────┤
        │ 81  │ 98  │ 86  │ 80 │ 99 │ 25 │ 8  │ 11 │ 16 │ 7  │
        ├─────┼─────┼─────┼────┼────┼────┼────┼────┼────┼────┤
        │ 84  │ 78  │ 91  │ 96 │ 77 │ 4  │ 20 │ 23 │ 3  │ 19 │
        ├─────┼─────┼─────┼────┼────┼────┼────┼────┼────┼────┤
        │ 87  │ 94  │ 82  │ 88 │ 93 │ 10 │ 15 │ 6  │ 9  │ 12 │
        └─────┴─────┴─────┴────┴────┴────┴────┴────┴────┴────┘
        
        
        BUILD SUCCESSFUL
        
        Total time: 1.048 secs

If you will pass arg "--ui" program will open small swing window to show dynamic process of filling result table

        $ ./gradlew check run -Pappargs="--ui"
        :compileJava UP-TO-DATE
        :processResources UP-TO-DATE
        :classes UP-TO-DATE
        :compileTestJava UP-TO-DATE
        :processTestResources UP-TO-DATE
        :testClasses UP-TO-DATE
        :test UP-TO-DATE
        :check UP-TO-DATE
        > Building 88% > :run
        
And the following window will be shown:
    
![](./img/form.png)
    
You can pass starting point with arg "--start (x,y)". This option can be combined with option "--ui"

        $ ./gradlew check run -Pappargs="--start (4,8)"
        :compileJava UP-TO-DATE
        :processResources UP-TO-DATE
        :classes UP-TO-DATE
        :compileTestJava UP-TO-DATE
        :processTestResources UP-TO-DATE
        :testClasses UP-TO-DATE
        :test UP-TO-DATE
        :check UP-TO-DATE
        :run
        ┌─────┬─────┬─────┬────┬────┬────┬────┬────┬────┬────┐
        │ 62  │ 59  │ 56  │ 65 │ 60 │ 43 │ 38 │ 32 │ 44 │ 37 │
        ├─────┼─────┼─────┼────┼────┼────┼────┼────┼────┼────┤
        │ 69  │ 53  │ 73  │ 70 │ 54 │ 27 │ 46 │ 41 │ 28 │ 34 │
        ├─────┼─────┼─────┼────┼────┼────┼────┼────┼────┼────┤
        │ 57  │ 66  │ 61  │ 58 │ 75 │ 49 │ 30 │ 36 │ 48 │ 31 │
        ├─────┼─────┼─────┼────┼────┼────┼────┼────┼────┼────┤
        │ 63  │ 71  │ 55  │ 64 │ 72 │ 42 │ 39 │ 33 │ 45 │ 40 │
        ├─────┼─────┼─────┼────┼────┼────┼────┼────┼────┼────┤
        │ 68  │ 52  │ 74  │ 67 │ 51 │ 26 │ 47 │ 50 │ 29 │ 35 │
        ├─────┼─────┼─────┼────┼────┼────┼────┼────┼────┼────┤
        │ 85  │ 79  │ 100 │ 97 │ 76 │ 1  │ 17 │ 24 │ 2  │ 18 │
        ├─────┼─────┼─────┼────┼────┼────┼────┼────┼────┼────┤
        │ 90  │ 95  │ 83  │ 89 │ 92 │ 22 │ 14 │ 5  │ 21 │ 13 │
        ├─────┼─────┼─────┼────┼────┼────┼────┼────┼────┼────┤
        │ 81  │ 98  │ 86  │ 80 │ 99 │ 25 │ 8  │ 11 │ 16 │ 7  │
        ├─────┼─────┼─────┼────┼────┼────┼────┼────┼────┼────┤
        │ 84  │ 78  │ 91  │ 96 │ 77 │ 4  │ 20 │ 23 │ 3  │ 19 │
        ├─────┼─────┼─────┼────┼────┼────┼────┼────┼────┼────┤
        │ 87  │ 94  │ 82  │ 88 │ 93 │ 10 │ 15 │ 6  │ 9  │ 12 │
        └─────┴─────┴─────┴────┴────┴────┴────┴────┴────┴────┘
        
        
        BUILD SUCCESSFUL
        
        Total time: 1.045 secs
  
Also if you can run this app as regular java app

        java -jar checkerboard.jar --help