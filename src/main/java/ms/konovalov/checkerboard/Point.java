package ms.konovalov.checkerboard;

import lombok.Data;

/**
 * Representation of immutable coordinates' tuple
 */
@Data(staticConstructor = "of")
public class Point {
    private final int x;
    private final int y;
}
