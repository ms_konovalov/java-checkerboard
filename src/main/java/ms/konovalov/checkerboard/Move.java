package ms.konovalov.checkerboard;

import lombok.EqualsAndHashCode;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Delegate;

import static ms.konovalov.checkerboard.Move.Allowed.*;

/**
 * Class representing one step on checkerboard and a list of allowed moves
 * {@link Move} contains of immutable {@link Point} and link on following {@link Move} if exists
 */
@EqualsAndHashCode
@ToString(includeFieldNames = false)
public class Move {

    /** Enumeration of allowed moves */
    enum Allowed {

        /** All allowed moves considering North on top of the screen while X axis directed down */
        N(0, -3), S(0, 3), E(3, 0), W(-3, 0),
        NW(-2, -2), NE(+2, -2), SW(-2, +2), SE(+2, +2);

        private final Point coordinates;

        Allowed(int x, int y) {
            this.coordinates = Point.of(x, y);
        }
    }

    /**
     * Delta of coordinates made by this move
     */
    @Delegate
    private final Point point;

    /**
     * Following {@link Move}, {@code null} if not exists
     */
    @Setter
    private Move next;

    public Move(Point p) {
        this.point = p;
    }

    /**
     * Returns following {@link Move} if exists
     *
     * @return following move or null
     */
    public Move next() {
        return next;
    }

    /**
     * Determine whether following move exists
     *
     * @return {@code true} if following move exists, otherwise {@code false}
     */
    public boolean hasNext() {
        return next != null;
    }

    /*
     * Factory methods to create {@link Move} to direction
     */
    static Move north() {
        return new Move(N.coordinates);
    }

    static Move south() {
        return new Move(S.coordinates);
    }

    static Move east() {
        return new Move(E.coordinates);
    }

    static Move west() {
        return new Move(W.coordinates);
    }

    static Move northwest() {
        return new Move(NW.coordinates);
    }

    static Move northeast() {
        return new Move(NE.coordinates);
    }

    static Move southwest() {
        return new Move(SW.coordinates);
    }

    static Move southeast() {
        return new Move(SE.coordinates);
    }
}
