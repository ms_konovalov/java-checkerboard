package ms.konovalov.checkerboard;

import com.beust.jcommander.*;
import de.vandermeer.asciitable.v2.RenderedTable;
import de.vandermeer.asciitable.v2.V2_AsciiTable;
import de.vandermeer.asciitable.v2.render.V2_AsciiTableRenderer;
import de.vandermeer.asciitable.v2.render.WidthAbsoluteEven;
import de.vandermeer.asciitable.v2.render.WidthLongestLine;
import de.vandermeer.asciitable.v2.render.WidthLongestWord;
import de.vandermeer.asciitable.v2.themes.V2_E_TableThemes;

import javax.swing.*;
import java.util.concurrent.Executors;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Main class to run solution
 */
public class PathFinder {

    private static final int UI_TRAVERSE_DELAY_MILLIS = 500;
    public static final int BOARD_SIZE = 10;

    /** Command Line argument to print help */
    @Parameter(names = "--help", help = true, description = "print this message")
    private boolean help = false;

    /** Command line argument to use simple swing ui */
    @Parameter(names = "--ui", description = "enables small swing table to demonstrate the process")
    private boolean ui = false;

    /** Command line to pass coordinates of starting cell */
    @Parameter(names = "--start",
            description = "coordinates of start point in format (x,y). If not passed start point will be chosen automatically",
            converter = PointConverter.class, validateWith = PointConverter.class)
    private Point start = null;

    public static void main(String... args) {
        PathFinder main = new PathFinder();
        JCommander jCommander = new JCommander(main);
        jCommander.setProgramName(PathFinder.class.getSimpleName());
        try {
            jCommander.parse(args);
            if (main.help) {
                jCommander.usage();
                return;
            }
            main.run();
        } catch (ParameterException pe) {
            System.out.println(pe.getMessage());
            jCommander.usage();
        }
    }

    /**
     * Run solution
     */
    private void run() {
        Path p = Path.createFullPath();
        if (ui) {
            runWithUI(p);
        } else {
            runWithConsole(p);
        }
    }

    /**
     * Run solution and print result into console
     *
     * @param path path how to go through the board
     */
    private void runWithConsole(Path path) {
        String[][] array = new String[BOARD_SIZE][BOARD_SIZE];
        path.traverse(start, (Point cell, String text) -> array[cell.getY()][cell.getX()] = text);
        printTable(array);
    }

    /**
     * Print table into console
     *
     * @param array two dimension array filled with sequence of moves
     */
    private void printTable(String[][] array) {
        V2_AsciiTable table = new V2_AsciiTable();
        table.addRule();
        for (String[] line : array) {
            table.addRow((Object[]) line);
            table.addRule();
        }
        V2_AsciiTableRenderer rend = new V2_AsciiTableRenderer();
        rend.setTheme(V2_E_TableThemes.UTF_LIGHT.get());
        rend.setWidth(new WidthLongestWord());
        RenderedTable rt = rend.render(table);
        System.out.println(rt);
    }

    /**
     * Run solution with simple swing UI
     *
     * @param path path how to go through the board
     */
    private void runWithUI(Path path) {
        UI window = createUI();
        Executors.newSingleThreadExecutor().submit(() ->
                path.traverse(start, (Point cell, String text) -> putValueIntoCell(window, text, cell.getX(), cell.getY()))
        );
    }

    /**
     * Create swing UI form
     *
     * @return created UI form
     */
    private UI createUI() {
        UI window = new UI(BOARD_SIZE, BOARD_SIZE);
        window.pack();
        window.setVisible(true);
        return window;
    }

    /**
     * Show each value on UI form
     *
     * @param ui ui form
     * @param value the number of made move
     * @param x column number
     * @param y row number
     */
    private static void putValueIntoCell(UI ui, String value, int x, int y) {
        try {
            Thread.sleep(UI_TRAVERSE_DELAY_MILLIS);
        } catch (InterruptedException ignored) {
        }
        SwingUtilities.invokeLater(() -> ui.setValueAt(value, x, y));
    }

    /**
     * Converter class for parsing and validating input args for start point
     */
    public static class PointConverter implements IStringConverter<Point>, IParameterValidator {

        Pattern pattern = Pattern.compile("[\\(](\\d),(\\d)[\\)]");

        @Override
        public Point convert(String value) {
            Matcher matcher = pattern.matcher(value);
            if (matcher.matches()) {
                return Point.of(Integer.parseInt(matcher.group(1)), Integer.parseInt(matcher.group(2)));
            }
            throw new ParameterException("Parameter 'start' value " + value + " is invalid");
        }

        @Override
        public void validate(String name, String value) throws ParameterException {
            if (!pattern.matcher(value).matches()) {
                throw new ParameterException("Parameter '" + name + "' value " + value + " is invalid");
            }
        }
    }
}
