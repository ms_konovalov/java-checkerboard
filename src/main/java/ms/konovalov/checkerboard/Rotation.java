package ms.konovalov.checkerboard;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

import static lombok.AccessLevel.PRIVATE;

/**
 * Class representing rotation of coordinates counter-clockwise with PI/2 step
 */
@RequiredArgsConstructor(access = PRIVATE)
@ToString(includeFieldNames = false, exclude = "degrees")
public class Rotation {

    /** value of angel in radians */
    private final double degrees;

    /** instance name for usability */
    private final String name;

    /** transformation on angel 0 degrees */
    public static Rotation NONE = new Rotation(Math.toRadians(0), "none");

    /** transformation on angel 90 degrees counter-clockwise */
    public static Rotation CC90 = new Rotation(Math.toRadians(90), "cc90");

    /** transformation on angel 90 degrees counter-clockwise */
    public static Rotation CC180 = new Rotation(Math.toRadians(180), "cc180");

    /** transformation on angel 90 degrees counter-clockwise */
    public static Rotation CC270 = new Rotation(Math.toRadians(270), "cc270");

    /**
     * Apply transformation to {@link Move} and return new instance of {@link Move}
     *
     * @param m initial instance to apply
     * @return resulted instance
     */
    public Move apply(Move m) {
        if (m == null) return null;
        return new Move(Point.of(countX(m.getX(), m.getY()), countY(m.getX(), m.getY())));
    }

    private int countX(int x, int y) {
        return (int)(x * Math.round(Math.cos(degrees)) + y * Math.round(Math.sin(degrees)));
    }

    private int countY(int x, int y) {
        return (int) (- x * Math.round(Math.sin(degrees)) + y * Math.round(Math.cos(degrees)));
    }
}
