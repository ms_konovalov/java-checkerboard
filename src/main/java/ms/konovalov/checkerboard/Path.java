package ms.konovalov.checkerboard;

import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;

import java.util.function.BiConsumer;
import java.util.function.Supplier;

/**
 * Class representing path to move over all cells on board
 */
@AllArgsConstructor
public class Path {

    private static final Point DEFAULT_START_POINT = Point.of(5, 5);
    private static final int MAX_COUNT = 100;

    /** First move, traverse will start from it */
    private Move first;
    /** Link to last move - necessary for joining several {@link Path}s */
    private Move last;

    /**
     * Traverse all the path from start point until finish or reaching MAX_COUNT
     *
     * @param start start point (ignored right now, DEFAULT_START_POINT is used instead)
     * @param addResult function will be executed to add result into some external storage
     */
    public void traverse(Point start, BiConsumer<Point, String> addResult) {
        new PathTraverse(start, addResult).traverse();
    }

    /**
     * Class that incapsulates traverse logic
     */
    @RequiredArgsConstructor
    private class PathTraverse {

        /** Desired start point from which counter will start incrementing */
        private final Point start;
        /** Fuction to put result into external storage */
        private final BiConsumer<Point, String> addResultConsumer;
        /** Flag indicates procedure reached desired start cell and counter must be incremented */
        private boolean startCellFound = false;
        /** Counter of met cells */
        private int count = 1;

        /**
         * Method traversing the path from default start cell and sending counted values to external storage when necessary
         */
        private void traverse() {
            if (start == null) {
                startCellFound = true;
            }
            tryAddResult(DEFAULT_START_POINT);
            Move move = first;
            Point cell = calculatePoint(DEFAULT_START_POINT, move);
            tryAddResult(cell);
            while (move.hasNext() && count <= MAX_COUNT) {
                move = move.next();
                cell = calculatePoint(cell, move);
                tryAddResult(cell);
            }
        }

        /**
         * Method determines wheather desired start cell is already met and in this case sends data to external storage
         *
         * @param cell current cell
         */
        private void tryAddResult(Point cell) {
            if (startCellFound || cell.equals(start)) {
                addResultConsumer.accept(cell, "" + count++);
                startCellFound = true;
            }
        }

        /**
         * Calculates next {@link Point} after {@link Move} made
         *
         * @param cell previous point
         * @param move made move
         * @return next point
         */
        private Point calculatePoint(Point cell, Move move) {
            return Point.of(cell.getX() + move.getX(), cell.getY() + move.getY());
        }
    }

    @RequiredArgsConstructor
    private static class PathBuilder {

        private final Rotation rotation;
        private Move first;
        private Move last;

        /**
         * Add new {@link Move} to constructing {@link Path}
         * @param provider provider of next {@link Move}
         * @return self
         */
        private PathBuilder go(Supplier<Move> provider) {
            Move move = rotation.apply(provider.get());
            if (last == null) {
                first = move;
                last = first;
            } else {
                last.setNext(move);
                last = move;
            }
            return this;
        }

        /* fluent methods to add moves into Path */

        private PathBuilder goN() {
            return go(Move::north);
        }

        private PathBuilder goS() {
            return go(Move::south);
        }

        private PathBuilder goE() {
            return go(Move::east);
        }

        private PathBuilder goW() {
            return go(Move::west);
        }

        private PathBuilder goNW() {
            return go(Move::northwest);
        }

        private PathBuilder goNE() {
            return go(Move::northeast);
        }

        private PathBuilder goSW() {
            return go(Move::southwest);
        }

        private PathBuilder goSE() {
            return go(Move::southeast);
        }

        /** add move after segment for another segment connection */
        private PathBuilder addTransition() {
            return goN();
        }

        /** Create reusable segment that covers 5x5 board */
        private PathBuilder createSegment() {
            return goE().goS().goW()
                    .goNE().goS().goNE()
                    .goW().goSE().goW()
                    .goNE().goSE().goN()
                    .goW().goS().goNE()
                    .goNW().goE().goS()
                    .goW().goNE().goW()
                    .goSE().goN().goSW();
        }

        private Path build() {
            return new Path(first, last);
        }
    }

    /**
     * Join with another {@link Path}
     *
     * @param path path to join
     * @return self
     */
    private Path join(Path path) {
        this.last.setNext(path.first);
        this.last = path.last;
        return this;
    }

    /**
     * Create Path segment that covers 5x5 board
     * @param r rotation
     * @return constructed {@link Path} segment
     */
    static Path createSegment(Rotation r) {
        return new PathBuilder(r).createSegment().build();
    }

    /**
     * Create Path segment that covers 5x5 board with additional move to transition to another segment
     * @param r rotation
     * @return constructed {@link Path} segment with transition move
     */
    static Path createSegmentWithTransition(Rotation r) {
        return new PathBuilder(r).createSegment().addTransition().build();
    }

    /**
     * Create cyclic {@link Path} that covers 10x10 board
     *
     * @return constructed {@link Path}
     */
    public static Path createFullPath() {
        Path startPath = Path.createSegmentWithTransition(Rotation.NONE);
        return startPath
                .join(Path.createSegmentWithTransition(Rotation.CC90))
                .join(Path.createSegmentWithTransition(Rotation.CC180))
                .join(Path.createSegmentWithTransition(Rotation.CC270))
                .join(startPath);
    }
}
