package ms.konovalov.checkerboard;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import java.awt.*;

/**
 * Simple Swing UI form representing plain table to demonstrate solution process
 */
public class UI extends JFrame {

    private static final int REGULAR_FONT_SIZE = 20;
    private static final int LONG_TEXT_FONT_SIZE = 16;
    private static final int CELL_WIDTH = 40;
    private static final int CELL_HEIGHT = 40;
    private JTable table;

    public UI(int rowCount, int columnCount) {
        setTitle("Checkerboard");
        setSize(columnCount * CELL_WIDTH, columnCount * CELL_HEIGHT);
        setLocationRelativeTo(null);

        TableModel dataModel = new DefaultTableModel(rowCount, columnCount);
        table = new JTable(dataModel);

        table.setFillsViewportHeight(true);

        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer(){
            @Override
            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                Component tableCellRendererComponent = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                if (value != null && value.toString().length() > 2) {
                    tableCellRendererComponent.setFont(new Font(table.getFont().getFamily(), Font.BOLD, LONG_TEXT_FONT_SIZE));
                }
                return tableCellRendererComponent;
            }
        };
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);

        for (int i = 0; i < table.getColumnModel().getColumnCount(); i++) {
            table.getColumnModel().getColumn(i).setMaxWidth(CELL_WIDTH);
            table.getColumnModel().getColumn(i).setCellRenderer(centerRenderer);
        }
        table.setRowHeight(CELL_HEIGHT);
        table.setFont(new Font(table.getFont().getFamily(), Font.PLAIN, REGULAR_FONT_SIZE));
        setLayout(new BorderLayout());
        add(table, BorderLayout.CENTER);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    /**
     * Set value to exact cell and fire event to update UI
     *
     * @param value String value
     * @param x column idx
     * @param y row idx
     */
    public void setValueAt(String value, int x, int y) {
        table.getModel().setValueAt(value, y, x);
    }
}
