package ms.konovalov.checkerboard;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import static ms.konovalov.checkerboard.Move.*;
import static ms.konovalov.checkerboard.Rotation.*;

@RunWith(DataProviderRunner.class)
public class MoveTest {

    @DataProvider
    public static Object[][] provideMovesAndExpectedMoveAfterRotation() {
        return new Object[][] {
                { north(), NONE, north()},
                { north(), CC90, west()},
                { north(), CC180, south()},
                { north(), CC270, east()},
                { west(), NONE, west()},
                { west(), CC90, south()},
                { west(), CC180, east()},
                { west(), CC270, north()},
                { south(), NONE, south()},
                { south(), CC90, east()},
                { south(), CC180, north()},
                { south(), CC270, west()},
                { east(), NONE, east()},
                { east(), CC90, north()},
                { east(), CC180, west()},
                { east(), CC270, south()},
        };
    }

    @Test
    @UseDataProvider( "provideMovesAndExpectedMoveAfterRotation" )
    public void testMoveAndRotation(Move in, Rotation r, Move expected) {
        Assert.assertEquals(expected, r.apply(in));
        Assert.assertEquals(expected, r.apply(in));
        Assert.assertEquals(expected, r.apply(in));
        Assert.assertEquals(expected, r.apply(in));
    }

}