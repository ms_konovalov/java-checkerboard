package ms.konovalov.checkerboard;

import org.junit.Assert;
import org.junit.Test;

public class PathTest {

    @Test
    public void testSegmentFills5x5Array() {
        Path path = Path.createSegment(Rotation.NONE);
        String[][] array = new String[5][5];
        path.traverse(null, (Point cell, String text) -> {
            Assert.assertTrue("Cell should be empty", array[cell.getY() - 5][cell.getX() - 5] == null);
            array[cell.getY() - 5][cell.getX() - 5] = text;
        });
        for (String[] row : array) {
            for (String cell : row) {
                Assert.assertTrue("All cells must be filled", cell != null);
            }
        }
    }

    @Test
    public void testPathFills10x10Array() {
        Path path = Path.createFullPath();
        String[][] array = new String[10][10];
        path.traverse(Point.of(0, 0), (Point cell, String text) -> {
            Assert.assertTrue("Cell should be empty", array[cell.getY()][cell.getX()] == null);
            array[cell.getY()][cell.getX()] = text;
        });
        for (String[] row : array) {
            for (String cell : row) {
                Assert.assertTrue("All cells must be filled", cell != null);
            }
        }
    }
}